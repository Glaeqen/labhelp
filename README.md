\*.sublime\* files --> put in *$HOME/.config/sublime-text-3/Packages/User* directory

* CLabsBuild --> Sublime Build config which implements make commands inside editor.
* SublimeGDB --> Simple config which says where GDB frontend plugin should search for executable file.

makefile --> Excellent makefile for lab project. Put inside your project dir. *@RobertGalat*

SublimeManual --> Handy manual.